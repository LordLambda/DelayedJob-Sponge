# DelayedJob for Sponge #

### What is DelayedJob? ###

DelayedJob is a SpongePlugin that is used to queue Async Jobs that neccisarrily don't need to run right away. This allows you to say "Hey do this later". The important part is DelayedJob shouldn't really touch SpongeCode. Sponge has two schedulers. Specifically the Synchronous scheduler, and the async scheduler. The synchronous scheduler doesn't really fit the same purpose of DelayedJob. As the whole point is to have it run like by itself, in the future. As for the competition again the scheduler service. As for the Async Scheduler Service why use DelayedJob in lieu of Scheduler Service? Well for one everything can be done async. You can queue jobs asynchrounsly by putting them in their own thread. Everything is thread-safe, from the point of queueing-jobs, and more. Don't want to wait to queue a job? Throw it in a thread that async queues the job cause why not?

It also allows you to set the thread size that is running at the current time. For example only want two threads processing delayed jobs? *Fine* change the configuration, who really gives a flying flajam. You want to change the timeout time? *Sure* just throw it on there, and stop being lazy. Do whatever your heart desires. One other benifit is the fact you can set the ThreadFactory. Don't like the built in ThreadFactory, and have one that does *souper* special things? ***That's Cool Bro***. DelayedJob Supports it *fully*. Last but not least it is *impossible* to have a job that fails because you've queued to many to fast. Cause we have a Fibonacci Heap to handle all yer data. No longer do you have to hope your job doesn't run.

The last benifit of using DelayedJob is the beautiful fact that you can set priorities. (The lower number priority the faster it runs, i.e. A Priority 1 Thread will run before a Priority 20 Thread). So Want specific jobs to run faster than others without doing all the hard work? Go ahead. Slap a delayed job on there, and have yourself a merry ol' time. Cause why not? Why *shouldn't* you be able to queue with a priority?

### How is DelayedJob Licensed? ###

DelayedJob is under the MIT liscense. Which basically means do whatever the crap you want with the code. I mean I'd prefer if you gave actual credit. But if you don't want too I mean. Whatever. Like. I mean. Whatever. It's a license. Deal with it. Or don't. I'm not really the boss of you. I mean I could be. That'd be weird though. Having you being converned by some random person on the internet. (If you can't tell I'm very tired at the time of writing this library, and README.md).

### What If I Want to Change DelayedJob? ###

Then File an Issue, or make a PR. Do whatever your heart desires. Just don't complain. If you don't gots an issue? Got it? I mean like. If you have a probably, that's cool bro. Submit an issue. Or Submit a PR. Explain yourself fully, and have it reviewed. If it gets denied/removed then complain if you have valid points to rebutt why it got rejected/not-implemented. Otherwise don't talk smack. Cause you have no room too. Kapiche?