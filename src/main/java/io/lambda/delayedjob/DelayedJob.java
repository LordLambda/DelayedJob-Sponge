package io.lambda.delayedjob;

import com.google.inject.Inject;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.hocon.HoconConfigurationLoader;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GamePreInitializationEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.service.config.DefaultConfig;

import java.io.File;
import java.io.IOException;

@Plugin(id = "delayed-job", name = "DelayedJob", version = "1.0.0")
public class DelayedJob {

  @Inject
  @DefaultConfig(sharedRoot = true)
  private File defaultConfig;

  private CommentedConfigurationNode configuration;
  private static DelayedJob instance;

  @Listener
  public void onPreInit(GamePreInitializationEvent preInitEvent) {
    instance = this;
    try {
      this.configuration = HoconConfigurationLoader.builder().setFile(defaultConfig).build().load();
    }catch(IOException e1) {
      e1.printStackTrace();
    }
    JobQueue.get().start();
  }

  public CommentedConfigurationNode getConfigValue(Object... path) {
    return this.configuration.getNode(path);
  }

  public static DelayedJob get() {
    return instance;
  }
}
