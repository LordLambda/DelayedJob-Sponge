package io.lambda.delayedjob;

import io.lambda.delayedjob.heaps.FibonacciHeap;
import io.lambda.delayedjob.priority.PriorityComparator;
import io.lambda.delayedjob.priority.PriorityRunnable;

import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class JobQueue extends Thread {

  private static JobQueue instance;

  private PriorityBlockingQueue<Runnable> queue;
  private ThreadPoolExecutor threadPool;
  private FibonacciHeap<Double, PriorityRunnable> fibonacciHeap;
  private Lock heapLock;
  private Lock threadPoolLock;

  protected JobQueue(DelayedJob job) {
    this.queue = new PriorityBlockingQueue<>(job.getConfigValue("DelayedJob", "maxsize").getInt(),
        new PriorityComparator<>());
    this.threadPool = new ThreadPoolExecutor(job.getConfigValue("DelayedJob", "coresize").getInt(),
        job.getConfigValue("DelayedJob", "maxsize").getInt(),
        job.getConfigValue("DelayedJob", "keepAliveTime").getLong(), TimeUnit.SECONDS, this.queue);
    this.heapLock = new ReentrantLock();
    this.threadPoolLock = new ReentrantLock();
    this.fibonacciHeap = new FibonacciHeap<>();
  }

  @Override
  public void run() {
    while(this.isAlive()) {
      if(queue.remainingCapacity() != 0) {
        heapLock.lock();
        int remaining = queue.remainingCapacity();
        for (int i = 0; i < remaining; ++i) {
          if(fibonacciHeap.getSize() != 0) {
            this.threadPoolLock.lock();
            threadPool.execute(fibonacciHeap.extractMinimum().getValue());
            this.threadPoolLock.unlock();
          }else {
            break;
          }
        }
        heapLock.unlock();
      }
    }
  }

  public void setThreadFactory(ThreadFactory factory) {
    this.threadPoolLock.lock();
    this.threadPool.setThreadFactory(factory);
    this.threadPoolLock.unlock();
  }

  public void queueJob(PriorityRunnable job) {
    this.heapLock.lock();
    this.fibonacciHeap.insert(job.getPriority(), job);
    this.heapLock.unlock();
  }

  public static JobQueue get() {
    if(instance == null) {
      synchronized (JobQueue.class) {
        if(instance == null) {
          instance = new JobQueue(DelayedJob.get());
        }
      }
    }
    return instance;
  }
}
