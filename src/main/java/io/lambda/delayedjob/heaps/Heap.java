package io.lambda.delayedjob.heaps;

import java.util.Comparator;
import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;

public interface Heap<TKey, TValue>
    extends Iterable<Heap.Entry<TKey, TValue>>  {

  public Comparator<? super TKey> getComparator();

  public int getSize();

  public boolean isEmpty();

  public boolean holdsEntry(Entry<TKey, TValue> entry)
      throws NullPointerException;

  public boolean containsEntry(Entry<TKey, TValue> entry)
      throws NullPointerException;

  public Entry<TKey, TValue> insert(TKey key, TValue value)
      throws ClassCastException, NullPointerException;

  public void insertAll(Heap<? extends TKey, ? extends TValue> other)
      throws NullPointerException, ClassCastException, IllegalArgumentException;

  public void union(Heap<TKey, TValue> other)
      throws ClassCastException, NullPointerException, IllegalArgumentException;

  public Entry<TKey, TValue> getMinimum()
      throws NoSuchElementException;

  public Entry<TKey, TValue> extractMinimum()
      throws NoSuchElementException;

  public void decreaseKey(Entry<TKey, TValue> e, TKey key)
      throws IllegalArgumentException, ClassCastException, NullPointerException;

  public void delete(Entry<TKey, TValue> e)
      throws IllegalArgumentException, NullPointerException;

  public void clear();

  public void forEach(Action<Heap.Entry<TKey, TValue>> action)
      throws NullPointerException;

  public Iterator<Heap.Entry<TKey, TValue>> iterator();

  public Collection<TKey> getKeys();

  public Collection<TValue> getValues();

  public Collection<Heap.Entry<TKey, TValue>> getEntries();

  public boolean equals(Object other);

  public int hashCode();

  public static interface Entry<TKey, TValue>
  {

    public TKey getKey();

    public TValue getValue();

    public TValue setValue(TValue value);

    public boolean equals(Object other);

    public int hashCode();

  }
}

