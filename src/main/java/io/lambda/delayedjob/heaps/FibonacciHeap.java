package io.lambda.delayedjob.heaps;

import java.util.Comparator;
import java.util.NoSuchElementException;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.io.Serializable;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.IOException;

public class FibonacciHeap<TKey, TValue> extends AbstractLinkedHeap<TKey, TValue> implements Serializable  {

  private static final long serialVersionUID = 9802348L;

  private final Comparator<? super TKey> comp;

  transient FibonacciHeapEntry<TKey, TValue> minimum;

  private transient int size;

  transient volatile int mod_count;

  private transient HeapReference source_heap;

  public FibonacciHeap()
  {
    this(null);
  }

  public FibonacciHeap(final Comparator<? super TKey> comp)
  {
    super();

    // Null min.
    minimum = null;
    size = 0;
    mod_count = 0;
    this.comp = comp;
    source_heap = new HeapReference(this);
  }

  @Override
  public Comparator<? super TKey> getComparator()
  {
    return comp;
  }

  @Override
  public int getSize()
  {
    return size;
  }

  @Override
  public boolean holdsEntry(final Heap.Entry<TKey, TValue> e)
      throws NullPointerException
  {
    if (e == null)
    {
      throw new NullPointerException();
    }

    // Obvious check.
    if (e.getClass().equals(FibonacciHeapEntry.class) == false)
    {
      return false;
    }

    // Narrow.
    FibonacciHeapEntry<TKey, TValue> entry = (FibonacciHeapEntry<TKey, TValue>) e;

    // Use reference trickery.
    return entry.isContainedBy(this);
  }

  @Override
  public Entry<TKey, TValue> insert(final TKey key, final TValue value)
      throws ClassCastException, NullPointerException
  {
    FibonacciHeapEntry<TKey, TValue> node = new FibonacciHeapEntry<TKey, TValue>(key, value, source_heap);

    // Do some node housekeeping.
    node.degree = 0;
    node.marked = false;
    node.left = node.right = node;
    node.parent = null;
    node.child = null;

    // Connect to root node.
    if (minimum == null)
    {
      minimum = node;
    }
    else
    {
      // Check for key compatibility before inserting.
      // May throw class cast...
      int cmp = compare(node, minimum);

      // Insert into root list.
      minimum.right.left = node;
      node.right = minimum.right;
      minimum.right = node;
      node.left = minimum;

      // We have a new winner...
      if (cmp < 0)
      {
        minimum = node;
      }
    }

    // Inc size
    size += 1;

    // Inc mod cout.
    mod_count += 1;

    // Return the new node.
    return node;
  }

  @Override
  public void union(final Heap<TKey, TValue> other)
      throws ClassCastException, NullPointerException, IllegalArgumentException
  {
    if (other == null)
    {
      throw new NullPointerException();
    }

    if (this == other)
    {
      throw new IllegalArgumentException();
    }

    if (other.isEmpty())
    {
      return;
    }

    if (other.getClass().equals(FibonacciHeap.class))
    {
      // Get other root.
      FibonacciHeap<TKey, TValue> that = (FibonacciHeap<TKey, TValue>) other;

      try
      {
        int cmp = 0;
        if (minimum != null && that.minimum != null)
        {
          // May throw class cast.
          cmp = compare(that.minimum, minimum);
        }

        // Cat root list of other heap together with this one's.
        minimum.left.right = that.minimum.right;
        that.minimum.right.left = minimum.left;
        minimum.left = that.minimum;
        that.minimum.right = minimum;

        if (cmp < 0)
        {
          // Point to new min.
          minimum = that.minimum;
        }

        // Update stuff.
        size += that.size;
        mod_count += 1;

        // Change that heap's heap reference to point to this heap.
        // Thus, all children of that become children of
        that.source_heap.setHeap(this);
        that.source_heap = new HeapReference(that);
      }
      finally
      {
        // Actually clear the other heap. Always done!
        that.clear();
      }
    }
    else
    {
      throw new ClassCastException();
    }
  }

  @Override
  public Entry<TKey, TValue> extractMinimum()
      throws NoSuchElementException
  {
    if (isEmpty())
    {
      throw new NoSuchElementException();
    }

    // References that will be needed. See CLRS.
    FibonacciHeapEntry<TKey, TValue> t;
    FibonacciHeapEntry<TKey, TValue> w;
    FibonacciHeapEntry<TKey, TValue> z = minimum;

    if (z.child != null)
    {
      // Remove parent references for all of z's children.
      w = z.child;
      t = w;

      do
      {
        t.parent = null;
        t = t.right;
      }
      while (t != w);

      // Add the children to the root list.
      minimum.left.right = w.right;
      w.right.left = minimum.left;
      minimum.left = w;
      w.right = minimum;
    }

    // Remove z from the root list.
    z.left.right = z.right;
    z.right.left = z.left;

    if (z == z.right)
    {
      // We hope the heap is now empty...
      minimum = null;
    }
    else
    {
      // We have some work to do.
      minimum = z.right;
      consolidate();
    }

    // Dec size, inc mod.
    size -= 1;
    mod_count += 1;

    // Clear old heap reference.
    z.clearSourceReference();

    // Return old minimum.
    return z;
  }

  @Override
  public Entry<TKey, TValue> getMinimum()
      throws NoSuchElementException
  {
    if (isEmpty())
    {
      throw new NoSuchElementException();
    }

    // Return it.
    return minimum;
  }

  @SuppressWarnings("unchecked")
  private void consolidate()
  {
    // Create the auxiliary array.
    int dn = (int) Math.floor(Math.log(size) / Math.log(2)) + 2;
    FibonacciHeapEntry[] a = new FibonacciHeapEntry[dn];

    // Iterating node - node at which to stop iterating...
    FibonacciHeapEntry<TKey, TValue> iter = minimum;

    // The node we're on now; w from CLRS.
    FibonacciHeapEntry<TKey, TValue> w = iter;

    // x and y from CLRS code.
    FibonacciHeapEntry<TKey, TValue> x;
    FibonacciHeapEntry<TKey, TValue> y;

    // temp ref.
    FibonacciHeapEntry<TKey, TValue> temp;

    // d from CLRS code.
    int d;

    do
    {
      x = w;
      d = x.degree;

      if (a[d] != x)
      {
        while (a[d] != null)
        {
          // y has same degree as x... This much we know.
          y = a[d];

          if (compare(y, x) < 0)
          {
            // Swap x and y.
            temp = x;
            x = y;
            y = temp;
          }

          // Make y a child of x.
          link(y, x);
          iter = x;
          w = x;
          a[d] = null;
          d += 1;
        }

        a[d] = x;
      }

      // Next node.
      w = w.right;
    }
    while (w != iter);

    // Reset... we need to iterate over the root list again.
    minimum = iter;
    w = iter;

    // Find the new minimum in the root list (if we don't already have it).
    do
    {
      if (compare(w, minimum) < 0)
      {
        // Found a new minimum node.
        minimum = w;
      }

      // Next.
      w = w.right;
    }
    while (w != iter);

  }

  private void link(final FibonacciHeapEntry<TKey, TValue> y, final FibonacciHeapEntry<TKey, TValue> x)
  {
    // Remove y from the root list.
    y.left.right = y.right;
    y.right.left = y.left;

    if (x.child == null)
    {
      // x is all alone in the world.
      y.right = y;
      y.left = y;
      x.child = y;
    }
    else
    {
      // Concat into child list of x.
      y.right = x.child.right;
      y.left = x.child;
      x.child.right.left = y;
      x.child.right = y;
    }

    // Some housekeeping for the nodes.
    y.parent = x;
    x.degree += 1;
    y.marked = false;
  }

  @Override
  public void decreaseKey(final Heap.Entry<TKey, TValue> e, final TKey k)
      throws IllegalArgumentException, ClassCastException
  {
    // Check and cast.
    if (holdsEntry(e) == false)
    {
      throw new IllegalArgumentException();
    }

    // x from CLRS.
    FibonacciHeapEntry<TKey, TValue> x = (FibonacciHeapEntry<TKey, TValue>) e;

    // Check key... May throw class cast as well.
    if (compareKeys(k, x.getKey()) > 0)
    {
      throw new IllegalArgumentException();
    }

    // Store the new key value.
    x.setKey(k);

    // Restore the heap structure.
    decreaseKeyImpl(x);
  }

  private void decreaseKeyImpl(final FibonacciHeapEntry<TKey, TValue> x)
  {
    // Get x's parent.
    FibonacciHeapEntry<TKey, TValue> y = x.parent;

    // If x has a lower key than it's parent (and assuming x was not already
    // in the root list) then we have work to do.
    if (y != null && compare(x, y) < 0)
    {
      cut(x, y);
      cascadingCut(y);
    }

    // See if the new node is smaller.
    if (compare(x, minimum) < 0)
    {
      minimum = x;
    }

    mod_count += 1;
  }

  private void cut(final FibonacciHeapEntry<TKey, TValue> x, final FibonacciHeapEntry<TKey, TValue> y)
  {
    if (x.right == x)
    {
      // Last child.
      y.child = null;
    }
    else
    {
      // Next yutz over.
      y.child = x.right;
    }

    // Remove x from the child list.
    x.left.right = x.right;
    x.right.left = x.left;

    // y has one less child.
    y.degree -= 1;

    // Add x to the root list.
    minimum.right.left = x;
    x.right = minimum.right;
    minimum.right = x;
    x.left = minimum;
    x.parent = null;

    // Unmark x, since it has just been cut.
    x.marked = false;
  }

  private void cascadingCut(final FibonacciHeapEntry<TKey, TValue> y)
  {
    FibonacciHeapEntry<TKey, TValue> z = y.parent;

    if (z != null)
    {
      if (y.marked == false)
      {
        // Simply mark y.
        y.marked = true;
      }
      else
      {
        // Otherwise, cut y and recursively cascade on z.
        cut(y, z);
        cascadingCut(z);
      }
    }
  }

  @Override
  public void delete(final Heap.Entry<TKey, TValue> e)
      throws IllegalArgumentException, NullPointerException
  {
    // Check and cast.
    if (holdsEntry(e) == false)
    {
      throw new IllegalArgumentException();
    }

    // Narrow.
    FibonacciHeapEntry<TKey, TValue> entry = (FibonacciHeapEntry<TKey, TValue>) e;

    // Make it infinitely small.
    entry.is_infinite = true;

    // Percolate the top,
    decreaseKeyImpl(entry);

    // Remove.
    extractMinimum();

    // Reset entry state.
    entry.is_infinite = false;
  }

  @Override
  public void clear()
  {
    // Clear lame fields.
    minimum = null;
    size = 0;
    mod_count += 1;

    // Clear the heap ref that all the existing nodes have been using.
    // All contained nodes now have null containing heap.
    source_heap.clearHeap();

    // Recreate the reference object.
    source_heap = new HeapReference(this);
  }

  @Override
  public Iterator<Heap.Entry<TKey, TValue>> iterator()
  {
    return new EntryIterator();
  }

  private void writeObject(final ObjectOutputStream out)
      throws IOException
  {
    out.defaultWriteObject();
    out.writeInt(size);

    // Write out all key/value pairs.
    Iterator<Heap.Entry<TKey, TValue>> it = new EntryIterator();
    Heap.Entry<TKey, TValue> et = null;
    while (it.hasNext())
    {
      try
      {
        et = it.next();

        // May result in NotSerializableExceptions, but we there's not a
        // whole helluva lot we can do about that.
        out.writeObject(et.getKey());
        out.writeObject(et.getValue());
      }
      catch (final ConcurrentModificationException cme)
      {
        // User's fault.
        throw (IOException) new IOException("Heap structure changed during serialization").initCause(cme);
      }
    }
  }

  @SuppressWarnings("unchecked")
  private void readObject(final ObjectInputStream in)
      throws IOException, ClassNotFoundException
  {
    // do the magic.
    in.defaultReadObject();

    // get comparator and size.
    int rsize = in.readInt();

    // Create new ref object.
    source_heap = new HeapReference(this);

    // Read and insert all the keys and values.
    TKey key;
    TValue value;
    for (int index = 0; index < rsize; index++)
    {
      key = (TKey) in.readObject();
      value = (TValue) in.readObject();
      insert(key, value);
    }
  }

  private class EntryIterator
      extends Object
      implements Iterator<Heap.Entry<TKey, TValue>>
  {

    private FibonacciHeapEntry<TKey, TValue> next;

    private final int my_mod_count;

    EntryIterator()
    {
      super();

      // Start at min.
      next = FibonacciHeap.this.minimum;

      // Copy mod count.
      my_mod_count = FibonacciHeap.this.mod_count;
    }

    @Override
    public boolean hasNext()
    {
      if (my_mod_count != FibonacciHeap.this.mod_count)
      {
        throw new ConcurrentModificationException();
      }

      return (next != null);
    }

    @Override
    public Heap.Entry<TKey, TValue> next()
        throws NoSuchElementException, ConcurrentModificationException
    {
      if (hasNext() == false)
      {
        throw new NoSuchElementException();
      }

      // Get the next node.
      FibonacciHeapEntry<TKey, TValue> n = next;
      next = getSuccessor(next);
      return n;
    }

    private FibonacciHeapEntry<TKey, TValue> getSuccessor(FibonacciHeapEntry<TKey, TValue> entry)
    {
      if (entry.child != null)
      {
        return entry.child;
      }
      FibonacciHeapEntry<TKey, TValue> first;

      do
      {
        first = (entry.parent == null) ? FibonacciHeap.this.minimum : entry.parent.child;

        // Look for siblings.
        if (entry.right != first)
        {
          return entry.right;
        }

        // Look at entry parent.
        entry = entry.parent;
      }
      while (entry != null);

      // Reached the root node, no more successors.
      return null;
    }

    @Override
    public void remove()
        throws UnsupportedOperationException
    {
      throw new UnsupportedOperationException();
    }

  }

  private static final class FibonacciHeapEntry<TKey, TValue>
      extends AbstractLinkedHeap.AbstractLinkedHeapEntry<TKey, TValue>
      implements Serializable
  {

    private static final long serialVersionUID = 2348L;

    transient boolean marked;

    transient int degree;

    transient FibonacciHeapEntry<TKey, TValue> parent;

    transient FibonacciHeapEntry<TKey, TValue> child;

    transient FibonacciHeapEntry<TKey, TValue> left;

    transient FibonacciHeapEntry<TKey, TValue> right;

    FibonacciHeapEntry(final TKey key, final TValue value, final HeapReference source_ref)
    {
      super(key, value, source_ref);
    }

  }
}
