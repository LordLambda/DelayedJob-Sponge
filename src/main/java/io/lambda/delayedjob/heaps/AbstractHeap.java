package io.lambda.delayedjob.heaps;

import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public abstract class AbstractHeap<TKey, TValue> implements Heap<TKey, TValue>, Iterable<Heap.Entry<TKey, TValue>> {
  static boolean objectEquals(final Object o1, final Object o2) {
    return (o1 == null) ? (o2 == null) : o1.equals(o2);
  }

  static int objectHashCode(final Object anObject) {
    return (anObject == null) ? 0 : anObject.hashCode();
  }

  private transient volatile Collection<TKey> keys;

  private transient volatile Collection<TValue> values;

  private transient volatile Collection<Heap.Entry<TKey, TValue>> entries;

  protected AbstractHeap() {
    super();
  }

  protected int compare(final Entry<TKey, TValue> node1,
                        final Entry<TKey, TValue> node2)
      throws ClassCastException, NullPointerException {
    return compareKeys(node1.getKey(), node2.getKey());
  }

  @SuppressWarnings("unchecked")
  protected int compareKeys(final TKey k1, final TKey k2)
      throws ClassCastException {
    return (getComparator() == null ? (((Comparable<TKey>) k1).compareTo(k2))
        : getComparator().compare(k1, k2));
  }

  @Override
  public void insertAll(final Heap<? extends TKey, ? extends TValue> other)
      throws NullPointerException, ClassCastException,
      IllegalArgumentException {
    if (other == null) {
      throw new NullPointerException();
    }

    if (other == this) {
      throw new IllegalArgumentException();
    }

    if (other.isEmpty()) {
      return;
    }

    // Loop over entries and stuff.
    Iterator<? extends Heap.Entry<? extends TKey, ? extends TValue>> it = other
        .getEntries().iterator();
    Entry<? extends TKey, ? extends TValue> entry;
    while (it.hasNext()) {
      entry = it.next();

      // Might throw class cast.
      insert(entry.getKey(), entry.getValue());
    }
  }

  @Override
  public boolean isEmpty() {
    return (getSize() == 0);
  }

  @Override
  public void forEach(final Action<Heap.Entry<TKey, TValue>> action)
      throws NullPointerException {
    if (action == null) {
      throw new NullPointerException();
    }

    // note that if action changes the state of this heap - and the
    // iterator detects concurrent modification - we'll die right here
    // with a ConcurrentModificationException (or something equally
    // sinister).
    Iterator<Heap.Entry<TKey, TValue>> entryIterator = iterator();
    while (entryIterator.hasNext()) {
      action.action(entryIterator.next());
    }
  }

  @SuppressWarnings("unchecked")
  @Override
  public boolean equals(final Object other) {
    if (other == null) {
      return false;
    }

    if (this == other) {
      return true;
    }

    if (Heap.class.isInstance(other) == false) {
      return false;
    }

    // erased cast... a little bit evil. We should also figure out
    // here if we want to just cast to Heap<K,V>.
    Heap<? extends TKey, ? extends TValue> that = (Heap<? extends TKey, ? extends TValue>) other;
    return getEntries().equals(that.getEntries());
  }

  @Override
  public int hashCode() {
    int code = 0;
    Iterator<Heap.Entry<TKey, TValue>> it = getEntries().iterator();
    while (it.hasNext()) {
      code += it.next().hashCode();
    }

    return code;
  }

  @Override
  public String toString() {
    StringBuilder buffer = new StringBuilder();
    buffer.append(getClass().getName());
    buffer.append("(");
    buffer.append(getSize());
    buffer.append(") ");
    buffer.append("[");

    Iterator<Heap.Entry<TKey, TValue>> it = getEntries().iterator();
    boolean next = it.hasNext();
    Heap.Entry<TKey, TValue> entry = null;
    TKey k = null;
    TValue v = null;
    while (next) {
      // Get next entry.
      entry = it.next();
      k = entry.getKey();
      v = entry.getValue();

      // Append mapping.
      buffer.append((k == this) ? "[self-reference]" : String.valueOf(k));
      buffer.append("->");
      buffer.append((v == this) ? "[self-reference]" : String.valueOf(v));

      // Check advance.
      next = it.hasNext();
      if (next) {
        buffer.append(", ");
      }
    }

    buffer.append("]");
    return buffer.toString();
  }

  @Override
  public boolean containsEntry(final Entry<TKey, TValue> entry)
      throws NullPointerException {
    if (entry == null) {
      throw new NullPointerException();
    }

    // Iterate over all entries...
    Iterator<Heap.Entry<TKey, TValue>> it = getEntries().iterator();
    Entry<TKey, TValue> next = null;
    while (it.hasNext()) {
      next = it.next();
      if (next.equals(entry)) {
        return true;
      }
    }

    // Nope.
    return false;
  }

  @Override
  public Collection<TKey> getKeys() {
    if (keys == null) {
      keys = new KeyCollection();
    }

    return keys;
  }

  @Override
  public Collection<TValue> getValues() {
    if (values == null) {
      values = new ValueCollection();
    }

    return values;
  }

  @Override
  public Collection<Heap.Entry<TKey, TValue>> getEntries() {
    if (entries == null) {
      entries = new EntryCollection();
    }

    return entries;
  }

  @SuppressWarnings("unchecked")
  @Override
  protected Object clone()
      throws CloneNotSupportedException {
    // May throw clone not supported.
    AbstractHeap<TKey, TValue> ah = (AbstractHeap<TKey, TValue>) super.clone();

    // Clear lame fields.
    ah.keys = null;
    ah.values = null;
    ah.entries = null;

    return ah;
  }

  private abstract class AbstractHeapCollection<TElement>
      extends AbstractCollection<TElement> {

    protected AbstractHeapCollection() {
    }

    @Override
    public final int size() {
      return getSize();
    }

    @Override
    public boolean add(final TElement objectToAdd)
        throws UnsupportedOperationException {
      throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(final Collection<? extends TElement> collectionToAdd)
        throws UnsupportedOperationException {
      throw new UnsupportedOperationException();
    }

    @Override
    public final void clear()
        throws UnsupportedOperationException {
      throw new UnsupportedOperationException();
    }

    @Override
    public final boolean remove(final Object objectToRemove)
        throws UnsupportedOperationException {
      throw new UnsupportedOperationException();
    }

    @Override
    public final boolean removeAll(final Collection<?> objectsToRemove)
        throws NullPointerException, UnsupportedOperationException {
      if (objectsToRemove == null) {
        throw new NullPointerException();
      }

      throw new UnsupportedOperationException();
    }

    @Override
    public final boolean retainAll(final Collection<?> objectsToRetain)
        throws NullPointerException, UnsupportedOperationException {
      if (objectsToRetain == null) {
        throw new NullPointerException();
      }

      throw new UnsupportedOperationException();
    }

    @Override
    public boolean contains(final Object objectToCheck) {
      // get an iterator over this collection.
      Iterator<TElement> iterator = iterator();

      while (iterator.hasNext()) {
        if (objectEquals(objectToCheck, iterator.next())) {
          return true;
        }

      }

      return false;
    }

    @SuppressWarnings("unchecked")
    @Override
    public final boolean equals(final Object other) {
      if (other == null) {
        return false;
      }

      if (other == this) {
        return true;
      }

      if (Collection.class.isInstance(other) == false) {
        return false;
      }

      // we have some work to do...

      // erased cast... here so that if/when Java ever starts retaining
      // generic type information at runtime, this method will be "more"
      // correct.
      Collection<TElement> that = (Collection<TElement>) other;

      // cheap check sizes.
      if (that.size() != size()) {
        return false;
      }

      // create shallow, mutable clones of both collections (this and
      // that). We use linked lists here because they support removal at
      // point (via their iterator) in O( 1 ) time, as well as O( 1 )
      // removal of the first element.
      List<TElement> itemsInThis = new LinkedList<TElement>(this);
      List<TElement> itemsInThat = new LinkedList<TElement>(that);

      boolean foundElement;
      TElement elementInThis;
      TElement elementInThat;
      Iterator<TElement> iteratorOverThat;
      while (itemsInThis.size() > 0) {
        // basically, we're going to start pulling elements off the
        // queue of elements in this collection, and make sure each one
        // exists in the list of elements in that. When we find one, we
        // remove it from the list of elements in that and repeat until
        // both lists are empty, or we don't find a corresponding
        // element.
        foundElement = false;
        elementInThis = itemsInThis.remove(0);
        iteratorOverThat = itemsInThat.iterator();

        while (iteratorOverThat.hasNext()) {
          elementInThat = iteratorOverThat.next();

          if (objectEquals(elementInThis, elementInThat)) {
            // remove element in that.
            iteratorOverThat.remove();

            // we found one equal - break to next element,
            foundElement = true;
            break;
          }
        }

        if (foundElement == false) {
          return false;
        }
      }

      // they must be equal!
      return true;
    }


    @Override
    public final int hashCode() {
      int hashCode = 0;

      Iterator<TElement> iterator = iterator();
      TElement next;
      while (iterator.hasNext()) {
        next = iterator.next();
        hashCode ^= objectHashCode(next);
      }

      return hashCode;
    }

  }

  private final class EntryCollection
      extends AbstractHeapCollection<Heap.Entry<TKey, TValue>> {

    EntryCollection() {
      super();
    }

    @Override
    public Iterator<Heap.Entry<TKey, TValue>> iterator() {
      return AbstractHeap.this.iterator();
    }

    @SuppressWarnings("unchecked")
    @Override
    public final boolean contains(final Object o) {
      if (o == null) {
        return false;
      }

      if (Entry.class.isAssignableFrom(o.getClass()) == false) {
        return false;
      }

      Heap.Entry<TKey, TValue> e = (Heap.Entry<TKey, TValue>) o;
      return containsEntry(e);
    }

  }

  private final class KeyCollection
      extends AbstractHeapCollection<TKey> {
    KeyCollection() {
      super();
    }

    @Override
    public Iterator<TKey> iterator() {
      return new KeyIterator();
    }

    private final class KeyIterator
        extends Object
        implements Iterator<TKey> {

      private final Iterator<Heap.Entry<TKey, TValue>> backingIterator;

      KeyIterator() {
        super();

        backingIterator = AbstractHeap.this.iterator();
      }

      @Override
      public boolean hasNext() {
        return backingIterator.hasNext();
      }

      @Override
      public TKey next() {
        return backingIterator.next().getKey();
      }

      @Override
      public void remove() {
        backingIterator.remove();
      }

    }

  }

  private final class ValueCollection
      extends AbstractHeapCollection<TValue> {

    ValueCollection() {
      super();
    }

    @Override
    public Iterator<TValue> iterator() {
      // Everything is implemented atop the entry collection iterator.
      return new ValueIterator();
    }

    private final class ValueIterator
        extends Object
        implements Iterator<TValue> {

      private final Iterator<Heap.Entry<TKey, TValue>> backingIterator;

      ValueIterator() {
        super();

        // get backing iterator.
        backingIterator = AbstractHeap.this.iterator();
      }

      @Override
      public boolean hasNext() {
        return backingIterator.hasNext();
      }

      @Override
      public TValue next() {
        return backingIterator.next().getValue();
      }

      @Override
      public void remove() {
        backingIterator.remove();
      }

    }

  }

  protected static abstract class AbstractHeapEntry<TKey, TValue>
      extends Object
      implements Heap.Entry<TKey, TValue> {
    private TKey key;

    private TValue value;

    protected AbstractHeapEntry(final TKey key, final TValue value) {
      super();

      // Store key and value.
      this.key = key;
      this.value = value;
    }

    @Override
    public final TKey getKey() {
      return key;
    }

    public final void setKey(final TKey key) {
      this.key = key;
    }

    @Override
    public final TValue getValue() {
      return value;
    }

    @Override
    public final TValue setValue(final TValue value) {
      TValue tmp = value;
      this.value = value;
      return tmp;
    }

    @SuppressWarnings("unchecked")
    @Override
    public final boolean equals(final Object other) {
      if (other == null) {
        return false;
      }

      if (this == other) {
        return true;
      }

      if (!Heap.Entry.class.isInstance(other)) {
        return false;
      }

      Heap.Entry that = (Heap.Entry) other;

      // Use happier version to check for null.
      return (objectEquals(key, that.getKey()) && objectEquals(
          value, that.getValue()));
    }

    @Override
    public final int hashCode() {
      return (objectHashCode(key) ^ objectHashCode(value));
    }

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder();
      sb.append(getKey() == this ? "[self-reference]" : String
          .valueOf(getKey()));
      sb.append("->");
      sb.append(getValue() == this ? "[self-reference]" : String
          .valueOf(getValue()));
      return sb.toString();
    }

  }
}