package io.lambda.delayedjob.heaps;

import java.lang.ref.WeakReference;

public abstract class AbstractLinkedHeap<TKey, TValue> extends AbstractHeap<TKey, TValue> {

  protected AbstractLinkedHeap()
  {
    super();
  }

  @Override
  protected int compare(final Entry<TKey, TValue> node1,
                        final Entry<TKey, TValue> node2)
      throws ClassCastException, NullPointerException
  {
    AbstractLinkedHeapEntry<TKey, TValue> e1 = (AbstractLinkedHeapEntry<TKey, TValue>) node1;
    AbstractLinkedHeapEntry<TKey, TValue> e2 = (AbstractLinkedHeapEntry<TKey, TValue>) node2;

    if (e1.is_infinite && e2.is_infinite)
    {
      // Probably shouldn't happen. A good indication of concurrent
      // modification... figure out if we should just toss an exception
      // here.
      return 0;
    }
    else if (e1.is_infinite)
    {
      return -1;
    }
    else if (e2.is_infinite)
    {
      return 1;
    }
    else
    {
      return super.compare(node1, node2);
    }
  }

  protected static abstract class AbstractLinkedHeapEntry<K, V>
      extends AbstractHeap.AbstractHeapEntry<K, V>
  {

    protected transient volatile boolean is_infinite;

    private transient volatile HeapReference containing_ref;

    protected AbstractLinkedHeapEntry(final K key, final V value,
                                      final HeapReference ref)
    {
      super(key, value);

      // Store ref.
      containing_ref = ref;
      is_infinite = false;
    }

    protected final boolean isContainedBy(final AbstractLinkedHeap<K, V> heap)
        throws NullPointerException
    {
      if (heap == null)
      {
        throw new NullPointerException();
      }

      if (containing_ref == null)
      {
        // Means that this node was orphaned from it's parent heap
        // via a clear or garbage collect.
        return false;
      }

      // Straight reference comparison.
      return (containing_ref.getHeap() == heap);
    }

    protected final void clearSourceReference()
    {
      containing_ref = null;
    }

  }

  @SuppressWarnings("unchecked")
  protected static final class HeapReference
      extends Object
  {

    private WeakReference<AbstractLinkedHeap> heap_ref;

    protected HeapReference(final AbstractLinkedHeap fh)
    {
      super();

      // Create stuff.
      heap_ref = new WeakReference<AbstractLinkedHeap>(fh);
    }

    protected final AbstractLinkedHeap getHeap()
    {
      return heap_ref.get();
    }

    protected final void setHeap(final AbstractLinkedHeap heap)
        throws NullPointerException
    {
      if (heap == null)
      {
        throw new NullPointerException();
      }

      // Clear ref.
      clearHeap();

      // Create new reference object.
      heap_ref = new WeakReference<AbstractLinkedHeap>(heap);
    }

    protected final void clearHeap()
    {
      heap_ref.clear();
    }

  }

}

