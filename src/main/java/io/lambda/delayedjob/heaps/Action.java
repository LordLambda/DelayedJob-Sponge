package io.lambda.delayedjob.heaps;

public interface Action<TInput> {
  public void action(TInput input);
}