package io.lambda.delayedjob.priority;

import java.util.Comparator;

public class PriorityComparator<T> implements Comparator<T> {
  @Override
  public int compare(T o1, T o2) {
    return ((Double) ((o1 instanceof PriorityRunnable) ? ((PriorityRunnable) o1).getPriority() : 20.0))
        .compareTo(((Double) ((o2 instanceof PriorityRunnable) ? ((PriorityRunnable) o2).getPriority() : 20.0)));
  }
}

