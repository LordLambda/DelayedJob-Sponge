package io.lambda.delayedjob.priority;

public abstract class PriorityRunnable implements Runnable, Comparable<PriorityRunnable> {

  private double priority;

  public PriorityRunnable(double priority) {
    this.priority = priority;
  }

  public Double getPriority() {
    return priority;
  }

  @Override
  public int compareTo(PriorityRunnable o) {
    return (this.priority > o.getPriority()) ? 1 : -1;
  }
}

